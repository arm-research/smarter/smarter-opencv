#!/usr/bin/env bash
set -e

# Config
MAINTAINER="Josh Minor"

if [ -z $VERSION ]; then
	VERSION="4.4.0"
fi

if [ -z $PYTHON_RUNTIME ]; then
	PYTHON_RUNTIME="3.7"
fi

CURR_DIR=$(pwd)

apt-get install -yqq build-essential wget cmake unzip pkg-config git
apt-get install -yqq libjpeg-dev libpng-dev libtiff-dev
apt-get install -yqq libavcodec-dev libavformat-dev libswscale-dev
apt-get install -yqq libv4l-dev libxvidcore-dev libx264-dev
apt-get install -yqq libgtk-3-dev
apt-get install -yqq libatlas-base-dev gfortran
apt-get install -yqq libgstreamer1.0-0 gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-tools libgstrtspserver-1.0-dev gstreamer1.0-rtsp gstreamer1.0-libav
python3 -m pip install numpy

# Build checkinstall
git clone https://github.com/giuliomoro/checkinstall
cd checkinstall
make install
cd ..
			
# Make a new directory
find $CURR_DIR/opencv-build ! -name "*.zip" -exec rm -r {} \ || true
mkdir -p $CURR_DIR/opencv-build
cd $CURR_DIR/opencv-build

# Download OpenCV
FILE=opencv.zip
if [ -f "$FILE" ]; then
    echo "$FILE exists"
else
    echo "$FILE does not exist"
    wget -O opencv.zip https://github.com/opencv/opencv/archive/${VERSION}.zip
    unzip -q opencv.zip
fi

FILE=opencv_contrib.zip
if [ -f "$FILE" ]; then
    echo "$FILE exists"
else
    echo "$FILE does not exist"
    wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/${VERSION}.zip
    unzip -q opencv_contrib.zip
fi

cd opencv-${VERSION}
# Make build directory.
mkdir -p build
# Change to 
cd build

cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D CMAKE_INSTALL_PREFIX=/usr/local \
	-D INSTALL_PYTHON_EXAMPLES=OFF \
	-D INSTALL_C_EXAMPLES=OFF \
	-D OPENCV_ENABLE_NONFREE=ON \
	-D ENABLE_NEON=ON \
	-D BUILD_LIST=imgcodecs,python3,video,videoio \
	-D ENABLE_TBB=ON \
	-D ENABLE=IPP=ON \
	-D ENABLE_VFVP3=ON \
	-D WITH_OPENMP=OFF \
	-D WITH_GSTREAMER=ON \
	-D WITH_CSTRIPES=ON \
	-D WITH_OPENCL=ON \
	-D ENABLE_FAST_MATH=1 \
	-D OPENCV_EXTRA_MODULES_PATH=$CURR_DIR/opencv-build/opencv_contrib-${VERSION}/modules \
	-D HAVE_opencv_python3=ON \
	-D PYTHON3_EXECUTABLE=$(which python3) \
	-D PYTHON3_DEFAULT_EXECUTABLE=$(which python3) \
	-D BUILD_opencv_python3=ON \
	-D PYTHON3_PACKAGES_PATH=/usr/local/lib/python${PYTHON_RUNTIME}/dist-packages \
	-D BUILD_EXAMPLES=OFF ..

make -j$(getconf _NPROCESSORS_ONLN)

checkinstall --default \
--type debian --install=no \
--pkgname opencv4 \
--pkgversion "${VERSION}" \
--pkglicense BSD \
--deldoc --deldesc --delspec \
--requires "libjpeg-dev,libpng-dev,libtiff-dev,libavcodec-dev,libavformat-dev,libswscale-dev,libv4l-dev,libxvidcore-dev,libx264-dev,libgtk-3-dev,libatlas-base-dev,gfortran,libgstreamer1.0-0,gstreamer1.0-plugins-base,gstreamer1.0-plugins-good,gstreamer1.0-plugins-bad,gstreamer1.0-plugins-ugly,gstreamer1.0-tools,libgstrtspserver-1.0-dev,gstreamer1.0-rtsp,gstreamer1.0-libav" \
--pakdir ~ --maintainer "${MAINTAINER}" --provides opencv4 \
--addso --autodoinst \
make install

mkdir /tmp/extract
mv /root/*deb /tmp/extract

