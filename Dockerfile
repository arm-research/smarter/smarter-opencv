ARG PYTHON_VERSION=3.7

FROM python:${PYTHON_VERSION}-buster

COPY build-opencv-deb.sh /build-opencv-deb.sh

ENV PYTHON_VERSION=${PYTHON_VERSION}

RUN apt update && ./build-opencv-deb.sh

ENTRYPOINT [ "bash" ]
